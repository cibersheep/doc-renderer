/*
 */

import QtQuick 2.9
import Ubuntu.Components 1.3

HeaderBase {
    signal searchText(string typedText)

    property bool isSearching: false

    contents: Row {
        anchors.centerIn: parent
        width: parent.width
        spacing: units.gu(1.5)

        Label {
            visible: !isSearching
            text: header.title
            textSize: Label.Large
            color: root.titleColor
        }

        TextField {
            id: searchField
            visible: isSearching
            width: parent.width

            // Disable predictive text
            inputMethodHints: Qt.ImhNoPredictiveText

            primaryItem: Icon {
                width: units.gu(2); height: width
                name: "find"
            }

            placeholderText: i18n.tr("Search key…")

            onTextChanged: searchText(text);
        }
    }

    leadingActionBar.actions: Action {
        iconName: "close"
        visible: isSearching
        text: i18n.tr("Close")

        onTriggered: closeHeader();
    }

    trailingActionBar {
        actions: [
            Action {
                iconName: "settings"
                text: i18n.tr("Settings")

                onTriggered: {
                    if (isSearching) {
                        closeHeader();
                    }

                    mainPageStack.push(Qt.resolvedUrl("PageSettings.qml"));
                }
            },
            Action {
                iconName: "search"
                text: i18n.tr("Search")
                visible: !isSearching

                onTriggered: {
                    isSearching = true;
                    searchField.focus = true;
                }
            }
        ]
    }

    function closeHeader() {
        searchField.text = "";
        isSearching = false;
        searchField.focus = false;
    }
}
