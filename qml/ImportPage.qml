import QtQuick 2.9
import Ubuntu.Components 1.3
import Ubuntu.Content 1.3

Page {
    id: importPage

    property var activeTransfer
    property bool allowMultipleFiles

    signal accept(var files)
    signal reject()

    ContentPeerPicker {
        anchors.fill: parent
        visible: true
        contentType: ContentType.All
        handler: ContentHandler.Source

        onPeerSelected: {
            if (importPage.allowMultipleFiles) {
                peer.selectionType = ContentTransfer.Multiple
            } else {
                peer.selectionType = ContentTransfer.Single
            }
            importPage.activeTransfer = peer.request()
            stateChangeConnection.target = importPage.activeTransfer
        }

        onCancelPressed: {
            reject();
        }
    }

    ContentTransferHint {
        anchors.fill: parent
        activeTransfer: importPage.activeTransfer
    }

    Connections {
        id: stateChangeConnection
        target: null
        onStateChanged: {
            if (importPage.activeTransfer.state === ContentTransfer.Charged) {
                var selectedItems = []
                for(var i in importPage.activeTransfer.items) {
                    selectedItems.push(String(importPage.activeTransfer.items[i].url).replace("file://", ""))
                }

                accept(selectedItems);
            }
        }
    }
}

