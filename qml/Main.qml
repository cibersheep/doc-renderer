import QtQuick 2.9
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Ubuntu.Content 1.3
import Morph.Web 0.1
import QtWebEngine 1.7
import Ubuntu.Content 1.3

import "Components" as Comps
MainView {
    id: root
    objectName: 'mainView'

    applicationName: 'docrenderer.cibersheep'

    anchorToKeyboard: true
    automaticOrientation: true

    property string urlPattern: 'www/*'
    property string gameImported: ""

    width: units.gu(50)
    height: units.gu(75)
    property real zoomFactor: units.gu(1) / 8

    property string lighterColor: '#f7f6f5'
    property string darkColor: '#0e8cba'
    property string lightColor: '#e0e0e0'

    property bool sortByModified: true
    property int marginColumn: units.gu(2)

    Component.onCompleted: {
        mainPageStack.push(web)
    }

    PageStack {
        id: mainPageStack
        anchors.fill: parent

        Page {
            id: web
            property string game

            header: Comps.HeaderMain {
                id: mainHeader
            }

            Component.onCompleted: webview.forceActiveFocus()

            WebContext {
                id: webcontext
                userAgent: 'Mozilla/5.0 (Linux; Ubuntu; Linux arm; rv:65.0) Gecko/20100101 Firefox/65.0'
                offTheRecord: false
            }

            WebView {
                id: webview
                context: webcontext
                url: Qt.resolvedUrl('www/index.html')
                focus: true

                anchors {
                    fill: parent
                    topMargin: mainHeader.height
                }

                settings {
                    javascriptCanAccessClipboard: true
                    localContentCanAccessFileUrls: true
                    localContentCanAccessRemoteUrls: true
                    allowRunningInsecureContent: true
                    allowWindowActivationFromJavaScript : true
                }

                Component.onCompleted: {
                    settings.localStorageEnabled = true;
                }

                zoomFactor: root.zoomFactor

                onFileDialogRequested: function(request) {
                    request.accepted = true;

                    var importPage = mainPageStack.push(Qt.resolvedUrl('ImportPage.qml'));
                    importPage.allowMultipleFiles = false; //(request.mode == FileDialogRequest.FileModeOpenMultiple);

                    importPage.accept.connect(function(files) {
                        request.dialogAccept(files);
                        mainPageStack.pop();
                    });

                    importPage.reject.connect(function() {
                        request.dialogReject();
                        mainPageStack.pop();
                    });
                }

            }

            ProgressBar {
                height: units.dp(3)

                anchors {
                    left: visible ? parent.left  : undefined
                    right: visible ? parent.right : undefined
                    top: parent.top
                }

                showProgressPercentage: false
                value: (webview.loadProgress / 100)
                visible: (webview.loading && !webview.lastLoadStopped)
            }
        }
    }
}
